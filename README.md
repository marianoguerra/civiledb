# Civile

A simple in-memory Key-Value Store built in Elixir using riak\_core's unir for that runs on top of partisan.

The code should work without modifications on riak\_core and riak\_core\_ng.

Check the tutorial for detailed instructions:

* Introduction: http://marianoguerra.org/posts/riak-core-on-partisan-on-elixir-tutorial-introduction.html
* Setup: http://marianoguerra.org/posts/riak-core-on-partisan-on-elixir-tutorial-setup.html
* Getting Started: http://marianoguerra.org/posts/riak-core-on-partisan-on-elixir-tutorial-getting-started.html

## License

MIT
